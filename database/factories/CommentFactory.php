<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Comment>
 */
class CommentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'email' => $this->faker->unique()->safeEmail(),
            'pseudo' => $this->faker->unique()->name(),
            'note' => rand(3, 5),
            'text' => $this->faker->text(),
            'created_at' => $this->faker->dateTimeBetween('-3 months', '-1 day'),
            'updated_at' => null
        ];
    }
}
