<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'email' => 'required|email|unique:comments',
            'pseudo' => 'required|unique:comments',
            'note' => 'required',
            'text' => 'required',
            'photo' => 'nullable|mimes:png,jpg,jpeg|max:2048',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        $requiredMessage = 'Ce champs est obligatoire';
        return [
            'email.required' => $requiredMessage,
            'email.email' => "Le format de l'adresse email n'est pas valide",
            'email.unique' => 'Cette adresse email a déjà été utilisée',
            'pseudo.required' => $requiredMessage,
            'pseudo.unique' => 'Ce pseudo a déjà été utilisé',
            'text.required' => 'Veuillez laisser un comentaire',
            'photo.mimes' => "La photo doit avoir l'extension png, jpg ou jpeg"
        ];
    }
}
