<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>


## Installation

### Dépendences php
`composer install`

### Dépendance JS
`npm i`

### Fichier d'environnement
Copier et renommer le fichier .env.example en .env
<br />`cp .env.example .env`

### Base de données
Installer le fichier de base de données (database.sqlite)
<br />`php artisan migrate`

### Compiler les assets
`npm run build`


## Peupler la BDD
### Enregistrer quelques données test
`php artisan db:seed`

## Démarrer le serveur
### Démarrer le serveur php ntégré de Laravel
`php artisan serve`

L'application sera lancée sur l'url localhost:8000*

\* Si le port 8000 est disponible sinon sur un autre port qui vous sera indiqué. 
